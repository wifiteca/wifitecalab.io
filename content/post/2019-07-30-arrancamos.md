---
title: ¡Tenemos blog!
subtitle: Arrancamos nuestra web
date: 2019-07-30
tags: ["hola", "inicio"]	
---
	
Por fin tenemos web. Gracias a [Hugo](https://gohugo.io/), un creador web estático y a [Gitlab](https://gitlab.com), un servicio de repositorios, tenemos una forma sencilla de publicar nuestros avances.
