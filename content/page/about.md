---
title: Sobre el proyecto
subtitle: Porqué Wifiteca
comments: false
---
![](/img/wifiteca3.png)

Wifiteca es un proyecto educativo para crear una biblioteca virtual inalámbrica realizado por los alumnos de Secundaria del Aula de Apoyo del [Colegio La Salle La Laguna](http://lasallelalaguna.es/).

Queremos proveer de una red wifi local con contenido para el aprendizaje. Nuestra idea es crear una biblioteca digital a través de una red wifi, uniendo Cultura Hacker, Software Libre y contenido Creative Commons.

Para ello hemos utilizado varios proyectos como [Pirate Box](https://piratebox.cc/) o [Lybrary Box](http://www.librarybox.us/) y adecuarlos a las necesidades de un colegio. Cambiamos el firmware de routers inalámbricos y lo personalizamos, añadiendo contenido libre con licencias [Creative Commons](https://creativecommons.org/) o de [Dominio Público](https://es.wikipedia.org/wiki/Dominio_p%C3%BAblico). 

Compartimos libros, imágenes, audios y canciones, aplicaciones o vídeos desde una wifi que gestiona todo este contenido que está en un pendrive conectado al router. No hay acceso a internet en sí, sólo al contenido que se muestra en una página desde el navegador de un móvil, una tablet, un ordenador o cualquier otro dispositivo que pueda conectarse vía wifi.
