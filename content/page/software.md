---
title: Software
subtitle: Qué pasos necesitas
comments: false
---
![](/img/routers.png)

Para tener tu propia wifiteca, necesitas realizar los siguientes pasos:

+ Fase Inicial: Descripción del proyecto, adquisición de los dispositivos y organización de las fases.

+ Fase Hacking: Conocer los componentes del dispositivo, cómo funcionan y flashear el router.

+ Fase Software Libre: Añadir el software el router para crear el portal web inalámbrico. Modificar el software a las necesidades del proyecto.

+ Fase Creative Commons: Buscar obras Creative Commons o de Dominio Público para generar un repositorio de contenido a Wifiteca. 

Los alumnos y alumnas de Secundaria del Aula de Apoyo del Colegio La Salle La Laguna han colaborado en todas las partes del proyecto, siendo protagonistas de cada una de las fases con el acompañamiento del profesor de Apoyo.
