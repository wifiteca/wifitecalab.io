## Bienvenido/a
![](/img/wifiteca1.png)

Te damos la bienvenida a este proyecto educativo.
Queremos realizar un proyecto colaborativo para proveer de una red wifi local con contenido para el aprendizaje.  
Nuestra idea es crear una biblioteca digital a través de la wifi, uniendo Cultura Hacking, Software Libre y contenido Creative Commons.

Si quieres colaborar con el proyecto, ponte en contacto con nosotros de las siguientes formas:

Correo: <wifiteca@gmail.com>  
Twitter: <https://twitter.com/wifiteca>  
Gitlab: <https://gitlab.com/wifiteca>  
Youtube: <https://www.youtube.com/wifiteca>  
